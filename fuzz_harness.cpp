#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>
#include <stdio.h>

int exec(const char* cmd) {
    // From https://stackoverflow.com/a/478960
    std::array<char, 128> buffer;
    std::string result;
    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
    if (!pipe) {
	return errno;
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
    }
    return 0;
}

// fuzz_target.cc
extern "C" int LLVMFuzzerTestOneInput(const uint8_t *Data, size_t Size) {

  FILE *f = fopen("test.cfg", "w");
  fwrite(Data, 1, Size, f);
  fclose(f);

  std::string command;
  command.append("./grub-mkimage -c test.cfg -p / -O i386 -o grub-test echo reboot normal");
  return exec(command.c_str());
}

