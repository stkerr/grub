./bootstrap

./autogen.sh

# Build all selected GRUB targets.
for target in $GRUB_TARGETS; do
    plat=${target#*-};
    arch=${target%-*};
    [ "$arch" = "arm64" ] && arch=aarch64-linux;
    [ "$arch" = "arm" ] && arch=arm-linux-gnueabi;
    [ "$arch" = "ia64" ] && arch=ia64-linux;
    [ "$arch" = "mipsel" ] && arch=mips64-linux;
    [ "$arch" = "powerpc" ] && arch=powerpc64-linux;
    [ "$arch" = "riscv32" ] && arch=riscv32-linux;
    [ "$arch" = "riscv64" ] && arch=riscv64-linux;
    [ "$arch" = "sparc64" ] && arch=sparc64-linux;
    echo "Building $target";
    mkdir obj-$target;
    JOBS=`getconf _NPROCESSORS_ONLN 2> /dev/null || echo 1`;
    [ "$JOBS" == 1 ] || JOBS=$(($JOBS + 1));
    ( cd obj-$target && ../configure --target=$arch --with-platform=$plat --prefix=/tmp/grub && make -j$JOBS && make -j$JOBS install ) &> log || ( cat log; false );
    done

# Our test canary.
echo -e "insmod echo\\ninsmod reboot\\necho hello world\\nreboot" > grub.cfg

export PATH=$PATH:./obj-i386-efi

# Assemble images and possibly run them.
for target in $GRUB_TARGETS; do grub-mkimage -c grub.cfg -p / -O $target -o grub-$target echo reboot normal; done

